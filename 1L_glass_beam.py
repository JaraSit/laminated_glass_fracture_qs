import fenics as fe
import numpy as np
import matplotlib.pyplot as plt
import math
import os
import utils.functions as fu


# Mesh creator
def prepare_mesh():
    mesh = fe.Mesh("Meshes/QS_4PB_1D_ref.xml")
    return mesh


def append_file(folder, file_name, files):
    file_temp = fe.XDMFFile(folder + "/" + file_name)
    file_temp.parameters["flush_output"] = True
    files.append(file_temp)


def prepare_files(folder, params, n_glass, n_layers):
    # Post-process files
    # File for displacement field
    files_u = []
    for i in range(n_layers):
        append_file(folder, "displ_u_" + str(i) + ".xdmf", files_u)
        append_file(folder, "displ_w_" + str(i) + ".xdmf", files_u)
        append_file(folder, "displ_phi_" + str(i) + ".xdmf", files_u)

    # File for damage field
    files_d = []
    for i in range(n_glass):
        append_file(folder, "damage_" + str(i) + ".xdmf", files_d)

    # Create log_file
    if not os.path.exists(folder):
        os.makedirs(folder)
    log_file = open(folder + "/log_file.txt", "w+")
    log_file.write("Log_file.txt\n")
    log_file.write(
        "eps_staggered = " + str(params.tol_sl) + ", max_iters_staggered = " + str(params.max_iter) +
        ", criterium = " + str(params.conv_crit) + ", True = converges\n")
    log_file.write("time\titer_numbers\tconverges\n")
    log_file.close()

    # Create stress_file
    stress_file = open(folder + "/stress_data.txt", "w+")
    stress_file.write("Stress_data.txt\n")
    stress_file.write(
        "S = Stress, E = Strain, T=Top surface in middle of beam, B=Bottom surface in middle of beam, TQ=Top surface in quarter ob beam\n")
    stress_file.write("time\tu_B\tE_x_B\tS_x_B\tE_x_T\tS_x_T\tE_x_QT\tS_x_QT\n")
    stress_file.close()

    # Create reacts_file
    react_file = open(folder + "/reaction_data.txt", "w+")
    react_file.write("Reaction_data.txt\n")
    react_file.write("time\tu_B\treaction\n")
    react_file.close()

    return files_u, files_d


def glass_1l_beam_solver(time_space, ft, folder, params, type_active, u_type, dec_type):

    # Definition of supporting point
    def left_point(x):
        return fe.near(x[0], l_off)

    # Definition of loading point
    def left_load_point(x):
        return fe.near(x[0], l_x / 2 - l_0 / 2)

    # Definition of symmetry axis location
    def symmetry_axis(x, on_boundary):
        return fe.near(x[0], l_x/2) and on_boundary

    # Staggered loop
    def solve_staggered():
        # Time loop
        en_prev = 0.0
        for i in range(0, len(time_space)):
            t = time_space[i]

            u_d.t = t

            # Informative print of time instant
            print("Time instant: ", t)

            ite = 1
            err = 50.0

            converges = False

            # Staggered loop
            while err > params.tol_sl:

                # Damage and displacement solutions
                solve_displacement()

                solve_damage()

                if params.conv_crit == "rel_inc":
                    # Damage and displacement increments
                    du.assign(u - u_old)
                    dd.assign(d - d_old)

                    du_split = du.split(deepcopy=True)
                    u_split = u.split(deepcopy=True)
                    err_u = fe.norm(du_split[1]) / fe.norm(u_split[1])

                    # Errors - damage is not standardized
                    #err_u = fe.norm(du) / fe.norm(u)
                    err_d = fe.norm(dd)
                    err = max(err_u, err_d)

                    print("iter", ite, "errors", err_u, err_d)

                    u_old.assign(u)
                    d_old.assign(d)
                elif params.conv_crit == "energy":
                    # en = get_total_energy(u, d)
                    # err = abs((en - en_prev) / en)
                    # print("iter", ite, ", energy_error: ", err)
                    # en_prev = en
                    raise Exception("Energy criterium not implement yet!")
                else:
                    raise Exception("conv_crit must be energy/rel_inc!")

                ite += 1

                # Max iterations condition
                if ite > params.max_iter:
                    print("max iterations reached")
                    break

                if err <= params.tol_sl:
                    converges = True

            d_min.assign(d)

            save_displacement_and_damage(file_u, file_d, t)

            save_stress_reaction_sd(t)

            file_object = open(folder + "/log_file.txt", "a")
            file_object.write(str(round(t, 5)) + "\t" + str(ite) + "\t" + str(converges) + "\n")
            file_object.close()

            if np.amax(d.vector()[:]) > 0.99:
                break

        [file_i.close() for file_i in file_u]
        [file_i.close() for file_i in file_d]

    # Displacement solver
    def solve_displacement():
        u.vector()[:] = np.random.rand(u.vector().size())
        H = fe.derivative(u_form, u, fe.TrialFunction(V))
        problem = fe.NonlinearVariationalProblem(u_form, u, BC_u, H)
        solver = fe.NonlinearVariationalSolver(problem)
        parameters = {"newton_solver": {"error_on_nonconvergence": False,
                                        "relative_tolerance": params.tol_nm}}
        solver.parameters.update(parameters)
        solver.solve()

    # Damage snes variational inequalities solver based on Newton's method
    def solve_damage():
        lower = d_min
        upper = fe.interpolate(fe.Constant(1.0), W)
        #upper = fe.interpolate(fe.Constant(1.0), W)

        # Solution of damage formulationw
        H = fe.derivative(d_form, d, fe.TrialFunction(W))

        snes_solver_parameters = {"nonlinear_solver": "snes",
                                  "snes_solver": {"linear_solver": "lu",
                                                  "relative_tolerance": 1.0e-6,
                                                  "absolute_tolerance": 1.0e-6,
                                                  "maximum_iterations": 50,
                                                  "report": True,
                                                  "error_on_nonconvergence": False,
                                                  "line_search": "basic"}}

        problem = fe.NonlinearVariationalProblem(d_form, d, BC_d, H)
        problem.set_bounds(lower, upper)

        solver = fe.NonlinearVariationalSolver(problem)
        solver.parameters.update(snes_solver_parameters)
        solver.solve()

    # Save displacements and damages into files
    def save_displacement_and_damage(file_u_i, file_d_i, t_i):
        u_ = u.split(deepcopy=True)
        file_u_i[0].write(u_[0], t_i)
        file_u_i[1].write(u_[1], t_i)
        file_u_i[2].write(u_[2], t_i)

        file_d_i[0].write(d, t_i)

    # Save stress and reaction to file
    def save_stress_reaction_sd(t_i):
        stress_file = open(folder + "/stress_data.txt", "a")
        u_, w_, phi_ = u.split(deepcopy=True)
        eps_bottom = u_.dx(0) + phi_.dx(0)*(-0.5*H)
        eps_top = u_.dx(0) + phi_.dx(0)*(0.5*H)
        strain_bottom = fu.local_project(eps_bottom, V0)
        strain_top = fu.local_project(eps_top, V0)
        stress_bottom = fu.local_project((1.0 - d)**2*E_glass*fu.mc_bracket(eps_bottom) - E_glass*fu.mc_bracket(-eps_bottom), V0)
        stress_top = fu.local_project((1.0 - d)**2*E_glass*fu.mc_bracket(eps_top) - E_glass*fu.mc_bracket(-eps_top), V0)
        s1 = strain_bottom(p_bottom)
        st1 = stress_bottom(p_bottom)
        s2 = strain_top(p_top)
        st2 = stress_top(p_top)
        s3 = 0.0
        st3 = 0.0
        disp = w_(p_top)
        stress_file.write(str(t_i) + "\t" + str(disp) + "\t" + str(s1) + "\t" + str(st1) + "\t" + str(s2) + "\t" + str(st2) + "\t" + str(s3) + "\t" + str(st3) + "\n")
        stress_file.close()
        react_file = open(folder + "/reaction_data.txt", "a")
        forces = fe.assemble(u_form)
        react = forces[r_dof]
        react_file.write(str(t_i) + "\t" + str(disp) + "\t" + str(react) + "\n")
        react_file.close()

    # Displacement form getter
    def get_u_form():
        # Selective integration for shearlock reduction
        dx_shear = fe.dx(metadata={"quadrature_degree": 0})

        u_tr, w_tr, phi_tr = fe.split(u)
        u_test, w_test, phi_test = fe.TestFunctions(V)

        num_int = np.linspace(-0.5*H, 0.5*H, n_ni)
        dh = abs(num_int[1] - num_int[0])

        A = b*H

        u_form = 0.0

        if u_type == "num_int":
            for i in range(len(num_int) - 1):
                gauss = 0.5*(num_int[i] + num_int[i + 1])
                eps_z = u_tr.dx(0) + phi_tr.dx(0)*gauss
                d_eps_z = u_test.dx(0) + phi_test.dx(0)*gauss
                eps_z_p = fu.mc_bracket(eps_z)
                eps_z_n = -fu.mc_bracket(-eps_z)
                u_form += fe.inner((1.0 - d)**2*E_glass*eps_z_p + E_glass*eps_z_n,d_eps_z)*dh*b*fe.dx
            u_form += w_test.dx(0)*G_glass*A*(w_tr.dx(0) + phi_tr)*dx_shear
            u_form += phi_test*G_glass*A*(phi_tr + w_tr.dx(0))*dx_shear
            u_form -= fe.Constant(0.0)*w_test*fe.dx
        elif u_type == "analytical":
            zN = -0.5*H*(d / (d - 2.0))
            gm = (1 - d)**2
            gp = fe.Constant(1.0)

            t1 = gm * (zN + 0.5 * H) + gp * (0.5 * H - zN)
            t2 = gm * (0.5 * zN ** 2 - 0.125 * H ** 2) + gp * (0.125 * H ** 2 - 0.5 * zN ** 2)
            t3 = gm * (zN ** 3 / 3.0 + H ** 3 / 24.0) + gp * (H ** 3 / 24.0 - zN ** 3 / 3.0)

            u_form += u_test.dx(0) * E_glass * t1 * b * u_tr.dx(0) * fe.dx + u_test.dx(0) * E_glass * t2 * b * phi_tr.dx(0) * fe.dx
            u_form += phi_test.dx(0) * E_glass * t2 * b * u_tr.dx(0) * fe.dx
            u_form += phi_test.dx(0) * E_glass * t3 * b * phi_tr.dx(0) * fe.dx
            # u_form += phi_test.dx(0)*E*self.cross.I*phi_.dx(0)*fe.dx

            u_form += w_test.dx(0) * G_glass * A * (w_tr.dx(0) + phi_tr) * dx_shear
            u_form += phi_test * G_glass * A * (phi_tr + w_tr.dx(0)) * dx_shear
            u_form -= fe.Constant(0.0) * w_test * fe.dx
        else:
            raise Exception("Type of beam model must be num_int/analytical/analytical_2!")
        return u_form

    # Return formulation for damage
    def get_d_form():
        d_test = fe.TestFunction(W)

        d_form = 0.0
        d_form += -2*get_energy_active(u, d)*fe.inner(1.0 - d, d_test)*fe.dx
        d_form += b*H*3.0/8.0*Gc_glass*(1.0/lc*d_test + 2*lc*fe.inner(fe.grad(d), fe.grad(d_test)))*fe.dx

        return d_form

    def get_energy_active(x, d):
        u_tr, w_tr, phi_tr = fe.split(x)
        en = 0.0
        num_int = np.linspace(-0.5*H, 0.5*H, n_ni)
        dh = abs(num_int[1] - num_int[0])

        if dec_type == "num_int":
            for i in range(len(num_int) - 1):
                gauss = 0.5 * (num_int[i] + num_int[i + 1])
                eps_z = u_tr.dx(0) + phi_tr.dx(0) * gauss
                eps_z_p = fu.mc_bracket(eps_z)
                temp = 0.5 * E_glass * eps_z_p ** 2 * dh
                en += temp
        elif dec_type == "surface":
            eps_i_1 = u_tr.dx(0) + phi_tr.dx(0) * 0.5 * H
            eps_i_2 = u_tr.dx(0) - phi_tr.dx(0) * 0.5 * H
            eps_max = fu.max_fce(eps_i_1, eps_i_2)
            en += 0.5 * fu.mc_bracket(eps_max) ** 2 * E_glass * H * b
        elif dec_type == "analytical":
            zN = -0.5 * H * (d / (d - 2.0))

            t1 = zN + 0.5 * H
            t2 = 0.5 * zN ** 2 - 0.125 * H ** 2
            t3 = zN ** 3 / 3.0 + H ** 3 / 24.0

            en += 0.5 * E_glass * t1 * b * (u_tr.dx(0)) ** 2
            en += E_glass * t2 * b * u_tr.dx(0) * phi_tr.dx(0)
            en += 0.5 * E_glass * t3 * b * (phi_tr.dx(0)) ** 2
        return en

    # --------------------
    # Parameters of task
    # --------------------
    # Material parameters
    E_glass = 70.0e9  # Young's modulus
    nu_glass = 0.22  # Poisson ratio
    G_glass = E_glass/2/(1+nu_glass)

    # Geometry parameters
    H = 0.02
    l_x, l_y = 1.1, H  # Length and thickness of glass beam
    l_off = 0.05  # Support offset
    l_0 = 0.2  # Pitch of load points
    b = 0.36
    n_ni = 40

    p_bottom = fe.Point((0.5*l_x, -0.5*H))
    p_top = fe.Point((0.5*l_x, 0.5*H))
    p_react = fe.Point(0.5 * l_x - 0.5 * l_0)

    # --------------------
    # Define geometry
    # --------------------
    mesh = prepare_mesh()

    # Fracture parameters
    h_min = mesh.hmin()
    lc = 2*h_min
    Gc_glass = lc * 8.0 / 3.0 * ft ** 2 / E_glass

    if type_active == "energy_mod":
        Gc_glass = lc*8.0/3.0*ft**2/E_glass/6.0

    fe.plot(mesh)
    plt.show()

    # --------------------
    # Function spaces
    # --------------------
    p1 = fe.FiniteElement("P", fe.interval, 1)
    element = fe.MixedElement([p1, p1, p1])
    V = fe.FunctionSpace(mesh, element)
    W = fe.FunctionSpace(mesh, "CG", 1)
    V0 = fe.FunctionSpace(mesh, "DG", 0)

    r_dof = fu.find_dof_beam(p_react, 1, V)

    # --------------------
    # Boundary conditions
    # --------------------
    u_d = fe.Expression("-t/1000.0", t=0.0, degree=0)  # Prescribed displacement
    BC_u_1 = fe.DirichletBC(V.sub(1), 0.0, left_point, method="pointwise")
    BC_u_2 = fe.DirichletBC(V.sub(1), u_d, left_load_point, method="pointwise")
    BC_u_3 = fe.DirichletBC(V.sub(0), 0.0, symmetry_axis)
    BC_u_4 = fe.DirichletBC(V.sub(2), 0.0, symmetry_axis)
    BC_u = [BC_u_1, BC_u_2, BC_u_3, BC_u_4]
    BC_d = []

    # --------------------
    # Initialization
    # --------------------
    u = fe.Function(V)
    u_old = fe.Function(V)
    du = fe.Function(V)
    d = fe.Function(W)
    d_old = fe.Function(W)
    dd = fe.Function(W)
    d_min = fe.Function(W)
    en_prev = 0.0

    file_u, file_d = prepare_files(folder, params, 1, 1)

    # --------------------
    # Forms
    # --------------------

    u_form = get_u_form()

    d_form = get_d_form()

    solve_staggered()


# Solver parameters
prms = fu.SolverParams()
prms.max_iter = 100
prms.tol_nm = 1.0e-11
prms.conv_crit = "rel_inc"

# Timespaces
time_space_beam = fu.make_time_space([0.1, 5.0, 5.9, 7.0], [0.1, 0.01, 0.001])

# Strengths of glass
ft_glass = 45.0e6

# Solution
#glass_1l_beam_solver(time_space_beam, ft_glass, "Solutions/1L_pham_beam_45", prms, "energy_mod", "analytical", "analytical")

# Data loading
beam_stress = np.loadtxt("Solutions/1L_pham_beam_45/stress_data.txt", skiprows=3)
beam_react = np.loadtxt("Solutions/1L_pham_beam_45/reaction_data.txt", skiprows=2)
plate_stress = np.loadtxt("Solutions/1L_plate_45/stress_data.txt", skiprows=3)
plate_react = np.loadtxt("Solutions/1L_plate_45/reaction_data.txt", skiprows=2)
ps_pham_stress = np.loadtxt("Solutions/1L_pham_ref/stress_data.txt", skiprows=3)
ps_pham_react = np.loadtxt("Solutions/1L_pham_ref/reaction_data.txt", skiprows=2)

# Graph - Stress comparison
plt.title("Tensile stresses")
plt.plot(beam_stress[:, 0], beam_stress[:, 3]/1.0e6, label="beam")
plt.plot(plate_stress[:, 0], plate_stress[:, 2]/1.0e6, label="plate")
plt.plot(ps_pham_stress[:, 0], ps_pham_stress[:, 2]/1.0e6, label="PS")
plt.legend()
plt.xlabel("Displacement w [mm]")
plt.ylabel("Stress [MPa]")
plt.show()

# Graph - Reaction comparison
plt.title("Reaction")
plt.plot(beam_react[:, 0], -2*beam_react[:, 2]/1.0e3, label="beam")
plt.plot(plate_react[:, 0], -4*plate_react[:, 1]/1.0e3, label="plate")
plt.plot(ps_pham_react[:, 0], -2*0.36*ps_pham_react[:, 1]/1.0e3, label="PS")
plt.legend()
plt.xlabel("Displacement w [mm]")
plt.ylabel("Reaction [kN]")
plt.show()
