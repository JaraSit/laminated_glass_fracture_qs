import fenics as fe
import numpy as np
import matplotlib.pyplot as plt
import math
import os
import utils.functions as fu


def prepare_mesh(l_x, l_y, l_0, ref_t):
    nx = 275
    ny = 10

    l_sym = 0.5 * l_x
    mesh = fe.RectangleMesh(fe.Point(0., 0.), fe.Point(l_sym, l_y), nx, ny, diagonal="right/left")

    # fe.plot(mesh, "Mesh")
    # plt.show()

    # Refine again
    markers = fe.MeshFunction("bool", mesh, 2)
    markers.set_all(False)
    for c in fe.cells(mesh):
        # Mark cells with facet midpoints near x == L/2
        for f in fe.facets(c):
            if fe.near(f.midpoint()[0], l_x / 2., 0.5 * (l_0 + 2.0 * l_y)):
                markers[c] = True
    mesh = fe.refine(mesh, markers, redistribute=False)

    # Refine again
    markers = fe.MeshFunction("bool", mesh, 2)
    markers.set_all(False)
    for c in fe.cells(mesh):
        # Mark cells with facet midpoints near x == L/2
        for f in fe.facets(c):
            if fe.near(f.midpoint()[0], l_x / 2., 0.5 * (l_0 + 2.0 * l_y)):
                markers[c] = True
    mesh = fe.refine(mesh, markers, redistribute=False)

    # Refine again
    markers = fe.MeshFunction("bool", mesh, 2)
    markers.set_all(False)
    for c in fe.cells(mesh):
        # Mark cells with facet midpoints near x == L/2
        for f in fe.facets(c):
            if fe.near(f.midpoint()[0], l_x / 2., 0.5 * (l_0 + 2.0 * l_y)):
                markers[c] = True
    mesh = fe.refine(mesh, markers, redistribute=False)

    if ref_t:
        # Refine again
        markers = fe.MeshFunction("bool", mesh, 2)
        markers.set_all(False)
        for c in fe.cells(mesh):
            # Mark cells with facet midpoints near x == L/2
            for f in fe.facets(c):
                if fe.near(f.midpoint()[0], 0.46, 0.01) or fe.near(f.midpoint()[1], 0.0, 0.0005):
                    markers[c] = True
        mesh = fe.refine(mesh, markers, redistribute=False)
        # Refine again
        markers = fe.MeshFunction("bool", mesh, 2)
        markers.set_all(False)
        for c in fe.cells(mesh):
            # Mark cells with facet midpoints near x == L/2
            for f in fe.facets(c):
                if fe.near(f.midpoint()[0], 0.46, 0.01) or fe.near(f.midpoint()[1], 0.0, 0.0005):
                    markers[c] = True
        mesh = fe.refine(mesh, markers, redistribute=False)

    return mesh


def prepare_mesh_2(l_x, l_y, l_0, ref_t):
    nx = 110
    ny = 4

    l_sym = 0.5 * l_x
    mesh = fe.RectangleMesh(fe.Point(0., 0.), fe.Point(l_sym, l_y), nx, ny, diagonal="left/right")

    # fe.plot(mesh, "Mesh")
    # plt.show()

    # Refine
    markers = fe.MeshFunction("bool", mesh, 2)
    markers.set_all(False)
    for c in fe.cells(mesh):
        # Mark cells with facet midpoints near x == L/2
        for f in fe.facets(c):
            if fe.near(f.midpoint()[0], l_x/2., 0.5 * (l_0 + 4.0 * l_y)):
                markers[c] = True
    mesh = fe.refine(mesh, markers, redistribute=False)

    # fe.plot(mesh, "Mesh")
    # plt.show()

    # Refine again
    markers = fe.MeshFunction("bool", mesh, 2)
    markers.set_all(False)
    for c in fe.cells(mesh):
        # Mark cells with facet midpoints near x == L/2
        for f in fe.facets(c):
            if fe.near(f.midpoint()[0], l_x / 2., 0.5 * (l_0 + 2.0 * l_y)):
                markers[c] = True
    mesh = fe.refine(mesh, markers, redistribute=False)

    # Refine again
    markers = fe.MeshFunction("bool", mesh, 2)
    markers.set_all(False)
    for c in fe.cells(mesh):
        # Mark cells with facet midpoints near x == L/2
        for f in fe.facets(c):
            if fe.near(f.midpoint()[0], l_x / 2., 0.5 * (l_0 + 2.0 * l_y)):
                markers[c] = True
    mesh = fe.refine(mesh, markers, redistribute=False)

    # Refine again
    markers = fe.MeshFunction("bool", mesh, 2)
    markers.set_all(False)
    for c in fe.cells(mesh):
        # Mark cells with facet midpoints near x == L/2
        for f in fe.facets(c):
            if fe.near(f.midpoint()[0], l_x / 2., 0.5 * (l_0 + 2.0 * l_y)):
                markers[c] = True
    mesh = fe.refine(mesh, markers, redistribute=False)

    if ref_t:
        # Refine again
        markers = fe.MeshFunction("bool", mesh, 2)
        markers.set_all(False)
        for c in fe.cells(mesh):
            # Mark cells with facet midpoints near x == L/2
            for f in fe.facets(c):
                if fe.near(f.midpoint()[0], 0.46, 0.01) or fe.near(f.midpoint()[1], 0.0, 0.0005):
                    markers[c] = True
        mesh = fe.refine(mesh, markers, redistribute=False)
        # Refine again
        markers = fe.MeshFunction("bool", mesh, 2)
        markers.set_all(False)
        for c in fe.cells(mesh):
            # Mark cells with facet midpoints near x == L/2
            for f in fe.facets(c):
                if fe.near(f.midpoint()[0], 0.46, 0.01) or fe.near(f.midpoint()[1], 0.0, 0.0005):
                    markers[c] = True
        mesh = fe.refine(mesh, markers, redistribute=False)

    return mesh


def prepare_files(folder, params):
    # Post-process files
    # File for displacement field
    file_u = fe.XDMFFile(folder + "/u.xdmf")
    file_u.parameters["flush_output"] = True

    # File for damage field
    file_d = fe.XDMFFile(folder + "/d.xdmf")
    file_d.parameters["flush_output"] = True

    # Create log_file
    if not os.path.exists(folder):
        os.makedirs(folder)
    log_file = open(folder + "/log_file.txt", "w+")
    log_file.write("Log_file.txt\n")
    log_file.write(
        "eps_staggered = " + str(params.tol_sl) + ", max_iters_staggered = " + str(params.max_iter) +
        ", criterium = " + str(params.conv_crit) + ", True = converges\n")
    log_file.write("time\titer_numbers\tconverges\n")
    log_file.close()

    # Create stress_file
    stress_file = open(folder + "/stress_data.txt", "w+")
    stress_file.write("Stress_data.txt\n")
    stress_file.write("S = Stress, E = Strain, T=Top surface in middle of beam, B=Bottom surface in middle of beam\n")
    stress_file.write("time\tE_x_B\tS_x_B\tE_x_T\tS_x_T\n")
    stress_file.close()

    # Create reacts_file
    react_file = open(folder + "/reaction_data.txt", "w+")
    react_file.write("Reaction_data.txt\n")
    react_file.write("time\treaction\n")
    react_file.close()

    return file_u, file_d


def glass_1l_solver(time_space, d_type, mesh_type, lc, ft, folder, params):

    # Definition of supporting point
    def left_point(x):
        return fe.near(x[0], l_off) and fe.near(x[1], 0.0)

    # Definition of loading point
    def left_load_point(x):
        return fe.near(x[0], l_x / 2 - l_0 / 2) and fe.near(x[1], l_y)

    # Definition of symmetry axis location
    def symmetry_axis(x, on_boundary):
        return fe.near(x[0], l_x/2) and on_boundary

    # Elastic stress tensor
    def sigma_el(u_i):
        return lmbda * (fe.tr(fu.eps(u_i))) * fe.Identity(2) + 2.0 * mu * (fu.eps(u_i))

    # Positive stress tensor - spectral decomposition
    def sigma_p_sd(u_i):
        return lmbda*(fe.tr(fu.eps_p(u_i)))*fe.Identity(2) + 2.0*mu*(fu.eps_p(u_i))

    # Negative stress tensor - spectral decomposition
    def sigma_n_sd(u_i):
        return lmbda*(fe.tr(fu.eps_n(u_i)))*fe.Identity(2) + 2.0*mu*(fu.eps_n(u_i))

    def get_total_energy(u_i, d_i):
        en_el_dens_1 = (1 - d_i)**2*psi_sd_plus(u_i) + psi_sd_minus(u_i)
        if d_type == "pham":
            en_el_dens_2 = 3.0/8.0*Gc_glass*(1.0/lc*d_i + lc*fe.inner(fe.grad(d_i), fe.grad(d_i)))
        elif d_type == "bourdin" or d_type == "miehe":
            en_el_dens_2 = 1.0/2.0*Gc_glass*(1.0/lc*d_i**2 + lc*fe.inner(fe.grad(d_i), fe.grad(d_i)))
        return fe.assemble(en_el_dens_1*fe.dx) + fe.assemble(en_el_dens_2*fe.dx)

    # Positive energy - spectral decomposition
    def psi_sd_plus(u_i):
        eps_sym = fu.eps(u_i)

        eps1 = (1. / 2.) * fe.tr(eps_sym) + fe.sqrt((1. / 4.) * (fe.tr(eps_sym) ** 2) - fe.det(eps_sym))
        eps2 = (1. / 2.) * fe.tr(eps_sym) - fe.sqrt((1. / 4.) * (fe.tr(eps_sym) ** 2) - fe.det(eps_sym))

        tr_eps_plus = fu.mc_bracket(fe.tr(eps_sym))
        sum_eps_plus_squared = fu.mc_bracket(eps1) ** 2 + fu.mc_bracket(eps2) ** 2

        return 0.5 * lmbda * tr_eps_plus ** 2 + mu * sum_eps_plus_squared

    # Negative energy - spectral decomposition
    def psi_sd_minus(u_i):
        eps_sym = fu.eps(u_i)

        eps1 = (1. / 2.) * fe.tr(eps_sym) + fe.sqrt((1. / 4.) * (fe.tr(eps_sym) ** 2) - fe.det(eps_sym))
        eps2 = (1. / 2.) * fe.tr(eps_sym) - fe.sqrt((1. / 4.) * (fe.tr(eps_sym) ** 2) - fe.det(eps_sym))

        tr_eps_minus = -fu.mc_bracket(-fe.tr(eps_sym))
        sum_eps_minus_squared = fu.mc_bracket(-eps1) ** 2 + fu.mc_bracket(-eps2) ** 2

        return 0.5 * lmbda * tr_eps_minus ** 2 + mu * sum_eps_minus_squared

    # Staggered loop
    def solve_staggered():
        # Time loop
        en_prev = 0.0
        for i in range(0, len(time_space)):
            t = time_space[i]

            u_d.t = t

            # Informative print of time instant
            print("Time instant: ", t)

            ite = 1
            err = 50.0

            converges = False

            # Staggered loop
            while err > params.tol_sl:

                # Damage and displacement solutions
                solve_displacement()
                solve_damage()

                if params.conv_crit == "rel_inc":
                    # Damage and displacement increments
                    du.assign(u - u_old)
                    dd.assign(d - d_old)

                    # Errors - damage is not standardized
                    err_u = fe.norm(du) / fe.norm(u)
                    err_d = fe.norm(dd)
                    err = max(err_u, err_d)

                    print("iter", ite, "errors", err_u, err_d)

                    u_old.assign(u)
                    d_old.assign(d)
                elif params.conv_crit == "energy":
                    en = get_total_energy(u, d)
                    err = abs((en - en_prev) / en)
                    print("iter", ite, ", energy_error: ", err)
                    en_prev = en
                else:
                    raise Exception("conv_crit must be energy/rel_inc!")

                ite += 1

                # Max iterations condition
                if ite > params.max_iter:
                    print("max iterations reached")
                    break

                if err <= params.tol_sl:
                    converges = True

            d_min.assign(d)
            file_u.write(u, t)
            file_d.write(d, t)

            save_stress_reaction(t)

            file_object = open(folder + "/log_file.txt", "a")
            file_object.write(str(round(t, 5)) + "\t" + str(ite) + "\t" + str(converges) + "\n")
            file_object.close()

        file_u.close()
        file_d.close()

    # Displacement solver
    def solve_displacement():
        u.vector()[:] = np.random.rand(u.vector().size())
        F = fe.derivative(psi_u_form, u, fe.TestFunction(V))
        H = fe.derivative(F, u, fe.TrialFunction(V))
        problem = fe.NonlinearVariationalProblem(F, u, BC_u, H)
        solver = fe.NonlinearVariationalSolver(problem)
        parameters = {"newton_solver": {"error_on_nonconvergence": False,
                                            "relative_tolerance": params.tol_nm}}
        solver.parameters.update(parameters)
        solver.solve()

    # Damage snes variational inequalities solver based on Newton's method
    def solve_damage():
        lower = d_min
        upper = fe.interpolate(fe.Constant(1.0), W)

        # Solution of damage formulationw
        H = fe.derivative(d_form, d, fe.TrialFunction(W))

        snes_solver_parameters = {"nonlinear_solver": "snes",
                                  "snes_solver": {"linear_solver": "lu",
                                                  "relative_tolerance": 1.0e-6,
                                                  "absolute_tolerance": 1.0e-6,
                                                  "maximum_iterations": 50,
                                                  "report": True,
                                                  "error_on_nonconvergence": False,
                                                  "line_search": "basic"}}

        problem = fe.NonlinearVariationalProblem(d_form, d, BC_d, H)
        problem.set_bounds(lower, upper)

        solver = fe.NonlinearVariationalSolver(problem)
        solver.parameters.update(snes_solver_parameters)
        solver.solve()

    # Save stress and reaction to file
    def save_stress_reaction(t_i):
        stress_file = open(folder + "/stress_data.txt", "a")
        strain = fu.local_project(fu.eps(u), V0)
        stress = fu.local_project((1-d)**2*sigma_p_sd(u) + sigma_n_sd(u), V0)
        s1 = strain(p_bottom)[0]
        st1 = stress(p_bottom)[0]
        s2 = strain(p_top)[0]
        st2 = stress(p_top)[0]
        stress_file.write(str(t_i) + "\t" + str(s1) + "\t" + str(st1) + "\t" + str(s2) + "\t" + str(st2) + "\n")
        stress_file.close()
        react_file = open(folder + "/reaction_data.txt", "a")
        weak_form = fe.derivative(psi_u_form, u, fe.TestFunction(V))
        forces = fe.assemble(weak_form)
        react = forces[r_dof]
        react_file.write(str(t_i) + "\t" + str(react) + "\n")
        react_file.close()

    # --------------------
    # Parameters of task
    # --------------------
    # Material parameters
    E_glass = 70.0e9  # Young's modulus
    nu_glass = 0.22  # Poisson ratio

    # Fracture parameters
    if d_type == "pham":
        Gc_glass = lc * 8.0 / 3.0 * ft ** 2 / E_glass
    elif d_type == "bourdin":
        Gc_glass = lc * 256.0 / 27.0 * ft ** 2 / E_glass
    elif d_type == "miehe":
        Gc_glass = lc * 256.0 / 27.0 * ft ** 2 / E_glass
    else:
        raise Exception("Type of damge must be pham/bourdin/miehe.")

    # Geometry parameters
    l_x, l_y = 1.1, 0.02  # Length and thickness of glass beam
    l_off = 0.05  # Support offset
    l_0 = 0.2  # Pitch of load points

    p_bottom = fe.Point(0.5*l_x, 0.0)
    p_top = fe.Point(0.5*l_x, l_y)
    p_react = fe.Point((0.5*l_x - 0.5*l_0, l_y))

    # Lames coefficients calculation
    lmbda = E_glass*nu_glass/(1 + nu_glass)/(1 - 2*nu_glass)
    mu = E_glass/2/(1 + nu_glass)
    lmbda = 2*mu*lmbda/(lmbda + 2*mu)

    # --------------------
    # Define geometry
    # --------------------
    if mesh_type == "mesh_ref":
        mesh = prepare_mesh(l_x, l_y, l_0, False)
    elif mesh_type == "mesh_unif":
        mesh = fe.RectangleMesh(fe.Point(0., 0.), fe.Point(0.5*l_x, l_y), 275, 10, "left/right")
    elif mesh_type == "mesh_ref_ref":
        mesh = prepare_mesh(l_x, l_y, l_0, True)
    else:
        raise Exception("Type of mesh must be mesh_unif or mesh_ref.")

    h_min = mesh.hmin()
    print(h_min)

    fe.plot(mesh)
    plt.show()

    # --------------------
    # Function spaces
    # --------------------
    V = fe.VectorFunctionSpace(mesh, "CG", 1)  # Function space for displacements
    V0 = fe.TensorFunctionSpace(mesh, "DG", 0)  # Function space for stress components
    W = fe.FunctionSpace(mesh, "CG", 1)  # Function space for damage
    r_dof = fu.find_dof(p_react, 1, V)  # DoF of reaction

    # --------------------
    # Boundary conditions
    # --------------------
    u_d = fe.Expression("-t/1000.0", t=0.0, degree=0)  # Prescribed displacement
    BC_u_1 = fe.DirichletBC(V.sub(1), 0.0, left_point, method="pointwise")
    BC_u_2 = fe.DirichletBC(V.sub(1), u_d, left_load_point, method="pointwise")
    BC_u_3 = fe.DirichletBC(V.sub(0), 0.0, symmetry_axis)
    BC_u = [BC_u_1, BC_u_2, BC_u_3]
    BC_d = []

    # --------------------
    # Initialization
    # --------------------
    u = fe.Function(V)
    u_old = fe.Function(V)
    du = fe.Function(V)
    d = fe.Function(W)
    d_old = fe.Function(W)
    dd = fe.Function(W)
    d_min = fe.Function(W)
    en_prev = 0.0

    file_u, file_d = prepare_files(folder, params)

    # --------------------
    # Forms
    # --------------------
    d_test = fe.TestFunction(W)

    if d_type == "pham":
        d_form = -2*psi_sd_plus(u)*fe.inner(1.0 - d, d_test)*fe.dx
        d_form += 3.0/8.0*Gc_glass*(1.0/lc*d_test + 2*lc*fe.inner(fe.grad(d), fe.grad(d_test)))*fe.dx
    elif d_type == "bourdin":
        d_form = -2*psi_sd_plus(u)*fe.inner(1.0 - d, d_test)*fe.dx
        d_form += Gc_glass*(1.0/lc*fe.inner(d, d_test) + lc*fe.inner(fe.grad(d), fe.grad(d_test)))*fe.dx
    elif d_type == "miehe":
        stress = sigma_el(u)
        sg00, sg01, sg10, sg11 = fu.eig_v(stress)
        D = fu.mc_bracket((fu.mc_bracket(sg00) / ft) ** 2 + (fu.mc_bracket(sg11) / ft) ** 2 - 1)
        d_form = -D*fe.inner(1.0 - d, d_test)*fe.dx
        d_form += (fe.inner(d, d_test) + lc**2*fe.inner(fe.grad(d), fe.grad(d_test)))*fe.dx
    else:
        raise Exception("Type of damage must be pham/bourdin/miehe.")

    psi_u_form = (1-d)**2*psi_sd_plus(u)*fe.dx + psi_sd_minus(u)*fe.dx

    solve_staggered()


time_space_pham_unif = fu.make_time_space([0.1, 5.5, 6.6, 8.2, 8.3], [0.5, 0.1, 0.01, 0.001])
time_space_bourdin_unif = fu.make_time_space([0.1, 5.5, 10.0, 15.0], [0.5, 0.1, 0.01])
time_space_miehe_unif = fu.make_time_space([0.1, 5.0, 6.0, 6.8, 7.1], [0.5, 0.1, 0.01, 0.001])
time_space_pham_ref = fu.make_time_space([0.1, 4.5, 5.6, 6.2, 6.5], [0.5, 0.1, 0.01, 0.001])
time_space_bourdin_ref = fu.make_time_space([0.1, 5.5, 10.0, 11.5], [0.5, 0.1, 0.01])
time_space_miehe_ref = fu.make_time_space([0.1, 5.0, 6.0, 6.74, 7.0], [0.5, 0.1, 0.01, 0.001])
time_space_pham_ref_ref = fu.make_time_space([0.1, 4.5, 5.6, 6.8], [0.5, 0.1, 0.01])

prms = fu.SolverParams()
prms.max_iter = 200
prms.tol_nm = 1.0e-11

# Solvers
#glass_1l_solver(time_space_pham_unif, "pham", "mesh_unif", 0.004, 45.0e6, "Solutions/1L_pham_unif", prms)
#glass_1l_solver(time_space_bourdin_unif, "bourdin", "mesh_unif", 0.004, 45.0e6, "Solutions/1L_bourdin_unif", prms)
#glass_1l_solver(time_space_miehe_unif, "miehe", "mesh_unif", 0.004, 45.0e6, "Solutions/1L_miehe_unif", prms)
#glass_1l_solver(time_space_pham_ref, "pham", "mesh_ref", 0.0005, 45.0e6, "Solutions/1L_pham_ref", prms)
#glass_1l_solver(time_space_bourdin_ref, "bourdin", "mesh_ref", 0.0005, 45.0e6, "Solutions/1L_bourdin_ref", prms)
#glass_1l_solver(time_space_miehe_ref, "miehe", "mesh_ref", 0.0005, 45.0e6, "Solutions/1L_miehe_ref", prms)
#glass_1l_solver(time_space_pham_ref_ref, "pham", "mesh_ref_ref", 0.0002, 45.0e6, "Solutions/1L_pham_ref_ref", prms)

# Loading of data on uniform mesh
pham_unif_stress = np.loadtxt("Solutions/1L_pham_unif/stress_data.txt", skiprows=3)
pham_unif_reacts = np.loadtxt("Solutions/1L_pham_unif/reaction_data.txt", skiprows=2)
miehe_unif_stress = np.loadtxt("Solutions/1L_miehe_unif/stress_data.txt", skiprows=3)
miehe_unif_reacts = np.loadtxt("Solutions/1L_miehe_unif/reaction_data.txt", skiprows=2)
bourdin_unif_stress = np.loadtxt("Solutions/1L_bourdin_unif/stress_data.txt", skiprows=3)
bourdin_unif_reacts = np.loadtxt("Solutions/1L_bourdin_unif/reaction_data.txt", skiprows=2)

# Loading of data on refined mesh
pham_ref_stress = np.loadtxt("Solutions/1L_pham_ref/stress_data.txt", skiprows=3)
pham_ref_reacts = np.loadtxt("Solutions/1L_pham_ref/reaction_data.txt", skiprows=2)
miehe_ref_stress = np.loadtxt("Solutions/1L_miehe_ref/stress_data.txt", skiprows=3)
miehe_ref_reacts = np.loadtxt("Solutions/1L_miehe_ref/reaction_data.txt", skiprows=2)
bourdin_ref_stress = np.loadtxt("Solutions/1L_bourdin_ref/stress_data.txt", skiprows=3)
bourdin_ref_reacts = np.loadtxt("Solutions/1L_bourdin_ref/reaction_data.txt", skiprows=2)

# Loading of data on super-refined mesh
pham_ref_ref_stress = np.loadtxt("Solutions/1L_pham_ref_ref/stress_data.txt", skiprows=3)
pham_ref_ref_reacts = np.loadtxt("Solutions/1L_pham_ref_ref/reaction_data.txt", skiprows=2)

# Graph - Development of localizaiton
plt.title("1L - tensile stresses")
plt.plot(pham_unif_stress[:, 0], pham_unif_stress[:, 2]/1.0e6, label="Pham unif")
plt.plot(pham_ref_stress[:, 0], pham_ref_stress[:, 2]/1.0e6, label="Pham ref")
plt.plot(pham_ref_ref_stress[:, 0], pham_ref_ref_stress[:, 2]/1.0e6, label="Pham ref 2")
plt.legend()
plt.xlabel("Prescribed displacement w [mm]")
plt.ylabel("Stress [MPa]")
plt.show()

# Graph - Reaction comparison on uniform mesh
plt.title("1L-PS uniform mesh - reactions")
plt.plot(pham_unif_reacts[:, 0], -pham_unif_reacts[:, 1]/1.0e3, label="Pham unif")
plt.plot(pham_ref_reacts[:, 0], -pham_ref_reacts[:, 1]/1.0e3, label="Pham ref")
plt.plot(pham_ref_ref_reacts[:, 0], -pham_ref_ref_reacts[:, 1]/1.0e3, label="Pham ref 2")
plt.legend()
plt.xlabel("Prescribed displacement w [mm]")
plt.ylabel("Reaction [kN]")
plt.show()

# Graph - Stress comparison on uniform mesh
plt.title("1L-PS uniform mesh - tensile stresses")
plt.plot(pham_unif_stress[:, 0], pham_unif_stress[:, 2]/1.0e6, label="Pham")
plt.plot(bourdin_unif_stress[:, 0], bourdin_unif_stress[:, 2]/1.0e6, label="Bourdin")
plt.plot(miehe_unif_stress[:, 0], miehe_unif_stress[:, 2]/1.0e6, label="Miehe")
plt.legend()
plt.xlabel("Prescribed displacement w [mm]")
plt.ylabel("Stress [MPa]")
plt.show()

# Graph - Reaction comparison on uniform mesh
plt.title("1L-PS uniform mesh - reactions")
plt.plot(pham_unif_reacts[:, 0], -pham_unif_reacts[:, 1]/1.0e3, label="Pham")
plt.plot(bourdin_unif_reacts[:, 0], -bourdin_unif_reacts[:, 1]/1.0e3, label="Bourdin")
plt.plot(miehe_unif_reacts[:, 0], -miehe_unif_reacts[:, 1]/1.0e3, label="Miehe")
plt.legend()
plt.xlabel("Prescribed displacement w [mm]")
plt.ylabel("Reaction [kN]")
plt.show()

# Graph - Stress comparison on refined mesh
plt.title("1L-PS refined mesh - stresses")
plt.plot(pham_ref_stress[:, 0], pham_ref_stress[:, 2]/1.0e6, label="Pham")
plt.plot(bourdin_ref_stress[:, 0], bourdin_ref_stress[:, 2]/1.0e6, label="Bourdin")
plt.plot(miehe_ref_stress[:, 0], miehe_ref_stress[:, 2]/1.0e6, label="Miehe")
plt.legend()
plt.xlabel("Prescribed displacement w [mm]")
plt.ylabel("Stress [MPa]")
plt.show()

# Graph - Reaction comparison on refined mesh
plt.title("1L-PS refined mesh - reactions")
plt.plot(pham_ref_reacts[:, 0], -pham_ref_reacts[:, 1]/1.0e3, label="Pham")
plt.plot(bourdin_ref_reacts[:, 0], -bourdin_ref_reacts[:, 1]/1.0e3, label="Bourdin")
plt.plot(miehe_ref_reacts[:, 0], -miehe_ref_reacts[:, 1]/1.0e3, label="Miehe")
plt.legend()
plt.xlabel("Prescribed displacement w [mm]")
plt.ylabel("Reaction [kN]")
plt.show()
