import numpy as np
import fenics as fe


# --------------------
# Utility functions
# --------------------

# Time space maker
def make_time_space(t_array, t_dens_array):
    t_space = np.array([])
    for i in range(len(t_array) - 1):
        t_i_end = t_array[i+1] - t_dens_array[i]
        t_i_steps = int((t_i_end - t_array[i])/t_dens_array[i])
        t_i_space = np.linspace(t_array[i], t_i_end, t_i_steps)
        t_space = np.concatenate((t_space, t_i_space))
    return t_space


# Projection on each element
def local_project(fce, space):
    lp_trial, lp_test = fe.TrialFunction(space), fe.TestFunction(space)
    lp_a = fe.inner(lp_trial, lp_test)*fe.dx
    lp_L = fe.inner(fce, lp_test)*fe.dx
    local_solver = fe.LocalSolver(lp_a, lp_L)
    local_solver.factorize()
    lp_f = fe.Function(space)
    local_solver.solve_local_rhs(lp_f)
    return lp_f


# Macauley bracket
def mc_bracket(v):
    return 0.5 * (v + abs(v))


# Maximum of two function with UFL
def max_fce(a, b):
    return 0.5*(a+b+abs(a-b))


def find_dof(p, d, v):
    found_dof = -1
    v_dofs = v.tabulate_dof_coordinates()
    v0_dofs = v.sub(d).dofmap().dofs()
    for i in range(0, len(v0_dofs)):
        v_x = v_dofs[v0_dofs[i], 0]
        v_y = v_dofs[v0_dofs[i], 1]
        if fe.near(v_x, p.x()) and fe.near(v_y, p.y()):
            found_dof = v0_dofs[i]
    print("Found_dof = ", found_dof)
    return found_dof


def find_dof_beam(p, d, v):
    found_dof = -1
    V_dofs = v.tabulate_dof_coordinates()
    V0_dofs = v.sub(d).dofmap().dofs()
    for i in range(0, len(V0_dofs)):
        v_x = V_dofs[V0_dofs[i]]
        if fe.near(v_x, p.x()):
            found_dof = V0_dofs[i]
    print("Found_dof = ", found_dof)
    return found_dof


def find_dofs(p_i, d_i, v):
    found_dofs = []
    v_dofs = v.tabulate_dof_coordinates()
    v0_dofs = v.sub(d_i).dofmap().dofs()
    for i in range(0, len(v0_dofs)):
        v_x = v_dofs[v0_dofs[i], 0]
        if fe.near(v_x, p_i.x()):
            found_dofs.append(v0_dofs[i])
    print("Found_dofs = ", found_dofs)
    return found_dofs


# --------------------
# Material models
# --------------------

# Symmetry gradient of displacements
def eps(v):
    return fe.sym(fe.grad(v))


# eigenvalues for 2x2 matrix
def eig_v(a):
    v00 = a[0, 0]/2 + a[1, 1]/2 - fe.sqrt(a[0, 0]**2 - 2*a[0, 0]*a[1, 1] + 4*a[0, 1]*a[1, 0] + a[1, 1]**2)/2
    v01 = 0.0
    v10 = 0.0
    v11 = a[0, 0]/2 + a[1, 1]/2 + fe.sqrt(a[0, 0]**2 - 2*a[0, 0]*a[1, 1] + 4*a[0, 1]*a[1, 0] + a[1, 1]**2)/2
    return v00, v01, v10, v11


# eigenvectors for 2x2 matrix
def eig_w(a):
    w00 = -a[0, 1]/(a[0, 0]/2 - a[1, 1]/2 + fe.sqrt(a[0, 0]**2 - 2*a[0, 0]*a[1, 1] + 4*a[0, 1]*a[1, 0] + a[1, 1]**2)/2)
    w01 = -a[0, 1]/(a[0, 0]/2 - a[1, 1]/2 - fe.sqrt(a[0, 0]**2 - 2*a[0, 0]*a[1, 1] + 4*a[0, 1]*a[1, 0] + a[1, 1]**2)/2)
    w10 = 1.0
    w11 = 1.0
    return w00, w01, w10, w11


# positive strain tensor
def eps_p(v):
    v00, v01, v10, v11 = eig_v(eps(v))
    v00 = fe.conditional(fe.gt(v00, 0.0), v00, 0.0)
    v11 = fe.conditional(fe.gt(v11, 0.0), v11, 0.0)
    w00, w01, w10, w11 = eig_w(eps(v))
    wp = ([w00, w01], [w10, w11])
    wp = fe.as_tensor(wp)
    vp = ([v00, v01], [v10, v11])
    vp = fe.as_tensor(vp)
    return wp*vp*fe.inv(wp)


# negative strain tensor
def eps_n(v):
    v00, v01, v10, v11 = eig_v(eps(v))
    v00 = fe.conditional(fe.lt(v00, 0.0), v00, 0.0)
    v11 = fe.conditional(fe.lt(v11, 0.0), v11, 0.0)
    w00, w01, w10, w11 = eig_w(eps(v))
    wn = ([w00, w01], [w10, w11])
    wn = fe.as_tensor(wn)
    vn = ([v00, v01], [v10, v11])
    vn = fe.as_tensor(vn)
    return wn*vn*fe.inv(wn)


# --------------------
# Auxiliary classes
# --------------------

class SolverParams:
    def __init__(self):
        self.tol_sl = 1.0e-6
        self.max_iter = 100
        self.conv_crit = "energy"
        self.tol_nm = 1.0e-10


class ViscoelasticMaterial:
    def __init__(self):
        self.Gs = None
        self.thetas = None
        self.nu = 0.49
        self.Ginf = 0.0
        self.c1 = 0.0
        self.c2 = 0.0
        self.Tref = 0.0
        self.Tact = 0.0
