## Laminated glass fracture - Support files

This git repository provides support for article "Phase-field fracture modelling of thin monolithic and laminated glass plates under quasi-static bending".

All source codes was written in Python 3 programming language with using external FEniCS library. Installation instructions are available on https://fenicsproject.org/download/. Execusion by command

```
python3 1L_glass.py
python3 1L_glass_plate.py
python3 1L_glass_beam.py
python3 3L_lg.py
python3 3L_lg_beam.py
```

Code generates several .xdmf files with solutions and it needs a few gigabytes of hard drive memory.

### Content of individual files
*1L_glass.py* - Solver for 2D plane stress 1L beams. Section "solution" must be uncommented to generate a solution file. Second part of code generates graphs from article.

*1L_glass_plate* - Generates solution file for 1-layer plate model.

*1L_glass_beam* - Generates solution file for 1-layer beam model and plot graphs from article. The solution from *1L_glass_plate* must already be generated for plotting graphs.

*3L_lg.py* - Generates solution files for 3-layer plain stress models for EVA and PVB foil. Second part of code generates graphs from article with experimental data comparison.

*3L_lg_beam.py* - Generates solution files for 3-layer beam model. 