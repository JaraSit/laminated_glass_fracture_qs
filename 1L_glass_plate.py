import fenics as fe
import numpy as np
import matplotlib.pyplot as plt
import math
import os
import utils.functions as fu


def prepare_mesh_quarter(l_x, l_y):
    nx = 11
    ny = 4

    l_sym = 0.5 * l_x
    mesh = fe.RectangleMesh(fe.Point(0., 0.), fe.Point(0.5*l_x, 0.5*l_y), nx, ny, diagonal="left/right")

    # fe.plot(mesh, "Mesh")
    # plt.show()

    # Refine
    markers = fe.MeshFunction("bool", mesh, 2)
    markers.set_all(False)
    for c in fe.cells(mesh):
        # Mark cells with facet midpoints near x == L/2
        for f in fe.facets(c):
            if f.midpoint()[0] > 0.0:
                markers[c] = True
    mesh = fe.refine(mesh, markers, redistribute=False)

    # fe.plot(mesh, "Mesh")
    # plt.show()

    # Refine again
    markers = fe.MeshFunction("bool", mesh, 2)
    markers.set_all(False)
    for c in fe.cells(mesh):
        # Mark cells with facet midpoints near x == L/2
        for f in fe.facets(c):
            if f.midpoint()[0] > 0.4:
                markers[c] = True
    mesh = fe.refine(mesh, markers, redistribute=False)

    # Refine again
    markers = fe.MeshFunction("bool", mesh, 2)
    markers.set_all(False)
    for c in fe.cells(mesh):
        # Mark cells with facet midpoints near x == L/2
        for f in fe.facets(c):
            if f.midpoint()[0] > 0.41:
                markers[c] = True
    mesh = fe.refine(mesh, markers, redistribute=False)

    # Refine again
    markers = fe.MeshFunction("bool", mesh, 2)
    markers.set_all(False)
    for c in fe.cells(mesh):
        # Mark cells with facet midpoints near x == L/2
        for f in fe.facets(c):
            if f.midpoint()[0] > 0.41:
                markers[c] = True
    mesh = fe.refine(mesh, markers, redistribute=False)

    # Refine again
    markers = fe.MeshFunction("bool", mesh, 2)
    markers.set_all(False)
    for c in fe.cells(mesh):
        # Mark cells with facet midpoints near x == L/2
        for f in fe.facets(c):
            if f.midpoint()[0] > 0.41:
                markers[c] = True
    mesh = fe.refine(mesh, markers, redistribute=False)

    return mesh


def prepare_mesh_quarter_3(l_x, l_y):
    nx = 11
    ny = 4

    l_sym = 0.5 * l_x
    mesh = fe.RectangleMesh(fe.Point(0., 0.), fe.Point(0.5*l_x, 0.5*l_y), nx, ny, diagonal="left/right")

    # fe.plot(mesh, "Mesh")
    # plt.show()

    # Refine
    markers = fe.MeshFunction("bool", mesh, 2)
    markers.set_all(False)
    for c in fe.cells(mesh):
        # Mark cells with facet midpoints near x == L/2
        for f in fe.facets(c):
            if f.midpoint()[0] > 0.35 or f.midpoint()[1] < 0.15:
                markers[c] = True
    mesh = fe.refine(mesh, markers, redistribute=False)

    # fe.plot(mesh, "Mesh")
    # plt.show()

    # Refine again
    markers = fe.MeshFunction("bool", mesh, 2)
    markers.set_all(False)
    for c in fe.cells(mesh):
        # Mark cells with facet midpoints near x == L/2
        for f in fe.facets(c):
            if f.midpoint()[0] > 0.4 or f.midpoint()[1] < 0.1:
                markers[c] = True
    mesh = fe.refine(mesh, markers, redistribute=False)

    # Refine again
    markers = fe.MeshFunction("bool", mesh, 2)
    markers.set_all(False)
    for c in fe.cells(mesh):
        # Mark cells with facet midpoints near x == L/2
        for f in fe.facets(c):
            if f.midpoint()[0] > 0.41 or f.midpoint()[1] < 0.05:
                markers[c] = True
    mesh = fe.refine(mesh, markers, redistribute=False)

    # Refine again
    markers = fe.MeshFunction("bool", mesh, 2)
    markers.set_all(False)
    for c in fe.cells(mesh):
        # Mark cells with facet midpoints near x == L/2
        for f in fe.facets(c):
            if f.midpoint()[0] > 0.41 or f.midpoint()[1] < 0.01:
                markers[c] = True
    mesh = fe.refine(mesh, markers, redistribute=False)

    # Refine again
    markers = fe.MeshFunction("bool", mesh, 2)
    markers.set_all(False)
    for c in fe.cells(mesh):
        # Mark cells with facet midpoints near x == L/2
        for f in fe.facets(c):
            if f.midpoint()[0] > 0.41 or f.midpoint()[1] < 0.005:
                markers[c] = True
    mesh = fe.refine(mesh, markers, redistribute=False)

    return mesh


def append_file(folder, file_name, files):
    file_temp = fe.XDMFFile(folder + "/" + file_name)
    file_temp.parameters["flush_output"] = True
    files.append(file_temp)


def prepare_files(folder, params, n_glass, n_layers):
    # Post-process files
    # File for displacement field
    files_u = []
    for i in range(n_layers):
        append_file(folder, "displ_u_" + str(i) + ".xdmf", files_u)
        append_file(folder, "displ_w_" + str(i) + ".xdmf", files_u)
        append_file(folder, "displ_phi_" + str(i) + ".xdmf", files_u)

    # File for damage field
    files_d = []
    for i in range(n_glass):
        append_file(folder, "damage_" + str(i) + ".xdmf", files_d)

    # Create log_file
    if not os.path.exists(folder):
        os.makedirs(folder)
    log_file = open(folder + "/log_file.txt", "w+")
    log_file.write("Log_file.txt\n")
    log_file.write(
        "eps_staggered = " + str(params.tol_sl) + ", max_iters_staggered = " + str(params.max_iter) +
        ", criterium = " + str(params.conv_crit) + ", True = converges\n")
    log_file.write("time\titer_numbers\tconverges\n")
    log_file.close()

    # Create stress_file
    stress_file = open(folder + "/stress_data.txt", "w+")
    stress_file.write("Stress_data.txt\n")
    stress_file.write("S = Stress, E = Strain, T=Top surface in middle of beam, B=Bottom surface in middle of beam\n")
    stress_file.write("time\tE_x_B\tS_x_B\tE_x_T\tS_x_T\n")
    stress_file.close()

    # Create reacts_file
    react_file = open(folder + "/reaction_data.txt", "w+")
    react_file.write("Reaction_data.txt\n")
    react_file.write("time\treaction\n")
    react_file.close()

    return files_u, files_d


def glass_1l_plate_solver(time_space, ft, folder, params, type_active, u_type, dec_type):

    # Definition of supporting point
    def left_point(x):
        return fe.near(x[0], l_off)

    # Definition of loading point
    def left_load_point(x):
        return fe.near(x[0], l_x / 2 - l_0 / 2)

    # Definition of symmetry axis location
    def symmetry_axis(x, on_boundary):
        return fe.near(x[0], l_x/2) and on_boundary

    def symmetry_axis_2(x, on_boundary):
        return fe.near(x[1], l_y / 2) and on_boundary

    # Positive stress tensor - spectral decomposition
    def sigma_p_sd(u_i):
        return lmbda*(fe.tr(fu.eps_p(u_i)))*fe.Identity(2) + 2.0*mu*(fu.eps_p(u_i))

    # Negative stress tensor - spectral decomposition
    def sigma_n_sd(u_i):
        return lmbda*(fe.tr(fu.eps_n(u_i)))*fe.Identity(2) + 2.0*mu*(fu.eps_n(u_i))

    def sigma_sd(u_i, d_i):
        k_res = fe.Constant(0.0)
        return ((1 - d_i) ** 2 + k_res) * sigma_p_sd(u_i) + sigma_n_sd(u_i)

    # Positive energy - spectral decomposition
    def psi_sd_plus(u_i):
        eps_sym = fu.eps(u_i)

        eps1 = (1. / 2.) * fe.tr(eps_sym) + fe.sqrt((1. / 4.) * (fe.tr(eps_sym) ** 2) - fe.det(eps_sym))
        eps2 = (1. / 2.) * fe.tr(eps_sym) - fe.sqrt((1. / 4.) * (fe.tr(eps_sym) ** 2) - fe.det(eps_sym))

        tr_eps_plus = fu.mc_bracket(fe.tr(eps_sym))
        sum_eps_plus_squared = fu.mc_bracket(eps1) ** 2 + fu.mc_bracket(eps2) ** 2

        return 0.5*lmbda*tr_eps_plus ** 2 + mu * sum_eps_plus_squared

    # Negative energy - spectral decomposition
    def psi_sd_minus(u_i):
        eps_sym = fu.eps(u_i)

        eps1 = (1. / 2.) * fe.tr(eps_sym) + fe.sqrt((1. / 4.) * (fe.tr(eps_sym) ** 2) - fe.det(eps_sym))
        eps2 = (1. / 2.) * fe.tr(eps_sym) - fe.sqrt((1. / 4.) * (fe.tr(eps_sym) ** 2) - fe.det(eps_sym))

        tr_eps_minus = -fu.mc_bracket(-fe.tr(eps_sym))
        sum_eps_minus_squared = fu.mc_bracket(-eps1) ** 2 + fu.mc_bracket(-eps2) ** 2

        return 0.5 * lmbda * tr_eps_minus ** 2 + mu * sum_eps_minus_squared

    # Staggered loop
    def solve_staggered():
        # Time loop
        en_prev = 0.0
        for i in range(0, len(time_space)):
            t = time_space[i]

            u_d.t = t

            # Informative print of time instant
            print("Time instant: ", t)

            ite = 1
            err = 50.0

            converges = False

            # Staggered loop
            while err > params.tol_sl:

                # Damage and displacement solutions
                solve_displacement()
                solve_damage()

                if params.conv_crit == "rel_inc":
                    # Damage and displacement increments
                    du.assign(u - u_old)
                    dd.assign(d - d_old)

                    # Errors - damage is not standardized
                    err_u = fe.norm(du) / fe.norm(u)
                    err_d = fe.norm(dd)
                    err = max(err_u, err_d)

                    print("iter", ite, "errors", err_u, err_d)

                    u_old.assign(u)
                    d_old.assign(d)
                elif params.conv_crit == "energy":
                    print("Not implemented")
                    # TODO: Not implemented yet
                    #en = get_total_energy(u, d)
                    #err = abs((en - en_prev) / en)
                    #print("iter", ite, ", energy_error: ", err)
                    #en_prev = en
                else:
                    raise Exception("conv_crit must be energy/rel_inc!")

                ite += 1

                # Max iterations condition
                if ite > params.max_iter:
                    print("max iterations reached")
                    break

                if err <= params.tol_sl:
                    converges = True

            d_min.assign(d)

            save_displacement_and_damage(file_u, file_d, t)

            save_stress_reaction(t)

            file_object = open(folder + "/log_file.txt", "a")
            file_object.write(str(round(t, 5)) + "\t" + str(ite) + "\t" + str(converges) + "\n")
            file_object.close()

        file_u.close()
        file_d.close()

    # Save displacements and damages into files
    def save_displacement_and_damage(file_u_i, file_d_i, t_i):
        u_ = u.split(deepcopy=True)
        file_u_i[0].write(u_[0], t_i)
        file_u_i[1].write(u_[1], t_i)
        file_u_i[2].write(u_[2], t_i)

        file_d_i[0].write(d, t_i)

    # Displacement solver
    def solve_displacement():
        u.vector()[:] = np.random.rand(u.vector().size())
        F = fe.derivative(psi_u_form, u, fe.TestFunction(V))
        H = fe.derivative(F, u, fe.TrialFunction(V))
        problem = fe.NonlinearVariationalProblem(F, u, BC_u, H)
        solver = fe.NonlinearVariationalSolver(problem)
        parameters = {"newton_solver": {"error_on_nonconvergence": False,
                                            "relative_tolerance": params.tol_nm}}
        solver.parameters.update(parameters)
        solver.solve()

    # Damage snes variational inequalities solver based on Newton's method
    def solve_damage():
        lower = d_min
        upper = fe.interpolate(fe.Constant(1.0), W)

        # Solution of damage formulationw
        H = fe.derivative(d_form, d, fe.TrialFunction(W))

        snes_solver_parameters = {"nonlinear_solver": "snes",
                                  "snes_solver": {"linear_solver": "lu",
                                                  "relative_tolerance": 1.0e-6,
                                                  "absolute_tolerance": 1.0e-6,
                                                  "maximum_iterations": 50,
                                                  "report": True,
                                                  "error_on_nonconvergence": False,
                                                  "line_search": "basic"}}

        problem = fe.NonlinearVariationalProblem(d_form, d, BC_d, H)
        problem.set_bounds(lower, upper)

        solver = fe.NonlinearVariationalSolver(problem)
        solver.parameters.update(snes_solver_parameters)
        solver.solve()

    # TODO: to do
    # Save stress and reaction to file
    def save_stress_reaction(t_i):
        stress_file = open(folder + "/stress_data.txt", "a")
        S = fe.as_tensor([[0.0, 1.0], [-1.0, 0.0]])
        u_, w_, theta_ = u.split(deepcopy=True)
        eps_bottom = fe.grad(u_) + S*fe.grad(theta_)*p_bottom.z()
        u_bottom = u_ + S * theta_ * p_bottom.z()
        eps_top = fe.grad(u_) + S * fe.grad(theta_) * p_top.z()
        u_top = u_ + S * theta_ * p_top.z()
        strain_bottom = fu.local_project(eps_bottom, V0)
        stress_bottom = fu.local_project(sigma_sd(u_bottom, d), V0)
        strain_top = fu.local_project(eps_top, V0)
        stress_top = fu.local_project(sigma_sd(u_top, d), V0)
        s1 = strain_bottom((p_bottom.x(), p_bottom.y()))[0]
        st1 = stress_bottom((p_bottom.x(), p_bottom.y()))[0]
        s2 = strain_top((p_top.x(), p_top.y()))[0]
        st2 = stress_top((p_top.x(), p_top.y()))[0]
        stress_file.write(str(t_i) + "\t" + str(s1) + "\t" + str(st1) + "\t" + str(s2) + "\t" + str(st2) + "\n")
        stress_file.close()

        react_file = open(folder + "/reaction_data.txt", "a")
        weak_form = fe.derivative(psi_u_form, u, fe.TestFunction(V))
        forces = fe.assemble(weak_form)

        react = 0.0
        for dof in r_dofs:
            react += forces[dof]
        react_file.write(str(t_i) + "\t" + str(react) + "\n")
        react_file.close()

    # Displacement form getter
    def get_psi_u_form():
        # Selective integration for shearlock reduction
        dx_shear = fe.dx(metadata={"quadrature_degree": 0})

        u_tr, w_tr, phi_tr = fe.split(u)
        S = fe.as_tensor([[0.0, 1.0], [-1.0, 0.0]])
        u_test, w_test, phi_test = fe.TestFunctions(V)

        num_int = np.linspace(-0.5*H, 0.5*H, n_ni)
        dh = abs(num_int[1] - num_int[0])

        u_energy = 0.0
        for i in range(len(num_int) - 1):
            gauss = 0.5*(num_int[i] + num_int[i + 1])
            u_z = u_tr + S*phi_tr*gauss
            u_energy += ((1 - d)**2*psi_sd_plus(u_z) + psi_sd_minus(u_z))*dh*fe.dx

        # Shear
        D2 = (E_glass*k_glass*H) / (2.0 * (1.0 + nu_glass))
        u_energy += 0.5*D2*fe.inner(fe.grad(w_tr) + S*phi_tr, fe.grad(w_tr) + S*phi_tr)*dx_shear
        # u_energy -= fe.Constant(100.0) * w_ * fe.dx

        return u_energy

    # Return formulation for damage
    def get_d_form():
        d_test = fe.TestFunction(W)
        u_tr, w_tr, phi_tr = fe.split(u)
        S = fe.as_tensor([[0.0, 1.0], [-1.0, 0.0]])

        # Positive enrgy
        u_z_l = u_tr + S*phi_tr*0.5*H
        u_z_u = u_tr - S*phi_tr*0.5*H
        en = fu.max_fce(psi_sd_plus(u_z_l)*H, psi_sd_plus(u_z_u)*H)

        d_form = 0.0
        d_form += -2*en*fe.inner(1.0 - d, d_test)*fe.dx
        d_form += H*3.0/8.0*Gc_glass*(1.0/lc*d_test + 2*lc*fe.inner(fe.grad(d), fe.grad(d_test)))*fe.dx

        return d_form

    # --------------------
    # Parameters of task
    # --------------------
    # Material parameters
    E_glass = 70.0e9  # Young's modulus
    nu_glass = 0.22  # Poisson ratio
    G_glass = E_glass/2/(1 + nu_glass)
    k_glass = 5.0/6.0

    # Lames coefficients calculation
    lmbda = E_glass * nu_glass / (1 + nu_glass) / (1 - 2 * nu_glass)
    mu = E_glass / 2 / (1 + nu_glass)
    lmbda = 2 * mu * lmbda / (lmbda + 2 * mu)

    # Geometry parameters
    H = 0.02
    l_x, l_y = 1.1, 0.36  # Length and thickness of glass beam
    l_off = 0.05  # Support offset
    l_0 = 0.2  # Pitch of load points
    b = 0.36
    n_ni = 40

    p_bottom = fe.Point((0.5*l_x, 0.5*l_y, -0.5*H))
    p_top = fe.Point((0.5*l_x, 0.5*l_y, 0.5*H))
    p_react = fe.Point(0.5*l_x - 0.5*l_0)

    # --------------------
    # Define geometry
    # --------------------
    mesh = prepare_mesh_quarter(l_x, l_y)

    # Fracture parameters
    h_min = mesh.hmin()
    lc = 2*h_min
    Gc_glass = lc*8.0/3.0*ft**2/E_glass

    fe.plot(mesh)
    plt.show()

    # --------------------
    # Function spaces
    # --------------------
    deg = 1
    deg_shear = 0
    p1 = fe.FiniteElement("P", fe.interval, 1)
    element = fe.MixedElement([fe.VectorElement('P', mesh.ufl_cell(), deg),
                               fe.FiniteElement('P', mesh.ufl_cell(), deg),
                               fe.VectorElement('P', mesh.ufl_cell(), deg)])
    V = fe.FunctionSpace(mesh, element)
    V0 = fe.TensorFunctionSpace(mesh, 'DG', 0)
    W = fe.FunctionSpace(mesh, "CG", 1)

    r_dofs = fu.find_dofs(p_react, 1, V)

    # --------------------
    # Boundary conditions
    # --------------------
    u_d = fe.Expression("-t/1000.0", t=0.0, degree=0)  # Prescribed displacement
    BC_u_1 = fe.DirichletBC(V.sub(1), 0.0, left_point)
    BC_u_2 = fe.DirichletBC(V.sub(1), u_d, left_load_point)
    BC_u_3 = fe.DirichletBC(V.sub(0).sub(0), 0.0, symmetry_axis)
    # BC_u_3a = fe.DirichletBC(V.sub(0).sub(1), 0.0, corner, method="pointwise")
    BC_u_4 = fe.DirichletBC(V.sub(2).sub(1), 0.0, symmetry_axis)
    BC_u_5 = fe.DirichletBC(V.sub(0).sub(1), 0.0, symmetry_axis_2)
    BC_u_6 = fe.DirichletBC(V.sub(2).sub(0), 0.0, symmetry_axis_2)
    BC_u = [BC_u_1, BC_u_2, BC_u_3, BC_u_4, BC_u_5, BC_u_6]
    BC_d = []

    # --------------------
    # Initialization
    # --------------------
    u = fe.Function(V)
    u_old = fe.Function(V)
    du = fe.Function(V)
    d = fe.Function(W)
    d_old = fe.Function(W)
    dd = fe.Function(W)
    d_min = fe.Function(W)
    en_prev = 0.0

    file_u, file_d = prepare_files(folder, params, 1, 1)

    # --------------------
    # Forms
    # --------------------
    psi_u_form = get_psi_u_form()

    d_form = get_d_form()

    solve_staggered()

# Solver parameters
prms = fu.SolverParams()
prms.max_iter = 100
prms.tol_nm = 1.0e-11
prms.conv_crit = "rel_inc"

# Timespaces
time_space_plate = fu.make_time_space([0.1, 5.2, 5.45, 5.55], [0.1, 0.02, 0.005])

# Strengths of glass
ft_glass = 45.0e6

# Solvers
glass_1l_plate_solver(time_space_plate, ft_glass, "Solutions/1L_plate_45", prms, "energy_mod", "analytical", "analytical")